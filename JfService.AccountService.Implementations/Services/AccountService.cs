﻿using JfService.AccountBalanceService.Abstractions;
using JfService.AccountBalanceService.Abstractions.Model;

namespace JfService.AccountService.Implementations.Services;

/// <summary>
/// Сервис работы с лицевыми счетами. 
/// </summary>
public class AccountService : IAccountService
{
    private readonly IBillingService _billingService;

    public AccountService(IBillingService billingService)
    {
        _billingService = billingService;
    }

    /// <summary>
    /// Получить таблицу взаиморасчётов по месяцам.
    /// </summary>
    /// <param name="accountId">Идентификатор лицевого счёта.</param>
    public async Task<IEnumerable<SettlementTableItem>>
        GetBalancesGroupedByMonth(int accountId)
    {
        IEnumerable<PaymentExt> accountPayments = (await _billingService.GetPaymentsAsync(accountId)).ToList();
        IEnumerable<Balance> accountBalances = (await _billingService.GetBalancesAsync(accountId))
            .OrderBy(accountBalance => accountBalance.Period)
            .ToList();

        List<SettlementTableItem> result = new List<SettlementTableItem>();

        if (!accountBalances.Any())
        {
            return result;
        }

        // Начальный баланс по ЛС-у.
        Decimal inBalance = accountBalances.First().InBalance;

        foreach (var accountBalance in accountBalances)
        {
            decimal totalPaymentSumInPeriod =
                GetTotalPaymentSumInPeriod(accountPayments, accountId, accountBalance.Period);
            decimal outBalanceInPeriod = inBalance + accountBalance.Calculation - totalPaymentSumInPeriod;

            var settlementTableItem = new SettlementTableItem(
                accountBalance.Period,
                inBalance,
                accountBalance.Calculation,
                totalPaymentSumInPeriod,
                outBalanceInPeriod
            );
            Console.WriteLine(settlementTableItem);

            result.Add(settlementTableItem);

            inBalance = outBalanceInPeriod;
        }

        return result.OrderByDescending(accountBalance => accountBalance.Period);
    }

    public async Task<IEnumerable<ReportItem>> GetBalancesGroupedByYear(int accountId)
    {
        IEnumerable<SettlementTableItem> items = await GetBalancesGroupedByMonth(accountId);

        List<ReportItem> groupedByYear =
            items.GroupBy(item => item.Period.Year)
                .Select(gr =>
                {
                    var firstMonth = gr.OrderBy(x => x.Period).First();
                    var lastMonth = gr.OrderBy(x => x.Period).Last();

                    return new ReportItem(
                        gr.Key.ToString(),
                        firstMonth.InBalance,
                        gr.Sum(s => s.Calculation),
                        gr.Sum(s => s.Payment),
                        lastMonth.OutBalance);
                })
                .OrderByDescending(item => item.PeriodName)
                .ToList();

        return groupedByYear;
    }

    public async Task<IEnumerable<ReportItem>> GetBalancesGroupedByQuarter(int accountId)
    {
        IEnumerable<SettlementTableItem> items = await GetBalancesGroupedByMonth(accountId);

        List<ReportItem> groupedByYear =
            items.GroupBy(item => new { item.Period.Year, item.Period.Quarter })
                .Select(gr =>
                {
                    var firstMonth = gr.OrderBy(x => x.Period).First();
                    var lastMonth = gr.OrderBy(x => x.Period).Last();

                    return new ReportItem(
                        $"{gr.Key.Year}Q{gr.Key.Quarter}",
                        firstMonth.InBalance,
                        gr.Sum(s => s.Calculation),
                        gr.Sum(s => s.Payment),
                        lastMonth.OutBalance);
                })
                .OrderByDescending(item => item.PeriodName)
                .ToList();

        return groupedByYear;
    }

    public async Task<IEnumerable<ReportItem>> GetBalances(int accountId, PeriodType periodType)
    {
        switch (periodType)
        {
            case PeriodType.Month:
                var items = await GetBalancesGroupedByMonth(accountId);
                return items.AsEnumerable().Select(ConvertToReportItem);
            case PeriodType.Quarter:
                return await GetBalancesGroupedByQuarter(accountId);
            case PeriodType.Year:
                return await GetBalancesGroupedByYear(accountId);
            default:
                throw new NotImplementedException($"Неизвестный тип периода {periodType}");
        }
    }

    private ReportItem ConvertToReportItem(SettlementTableItem settlementTableItem)
    {
        return new ReportItem(
            settlementTableItem.Period.ToString(),
            settlementTableItem.InBalance,
            settlementTableItem.Calculation,
            settlementTableItem.Payment,
            settlementTableItem.OutBalance);
    }

    /// <summary>
    /// Получить общую сумму платежей в периоде.
    /// </summary>
    /// <param name="accountPayments">Платежи.</param>
    /// <param name="accountId">Идентификатор ЛС.</param>
    /// <param name="accountBalancePeriod">Период.</param>
    private decimal GetTotalPaymentSumInPeriod(IEnumerable<PaymentExt> accountPayments, int accountId,
        Period accountBalancePeriod)
    {
        Decimal totalSumInPeriod = accountPayments
            .Where(accountPayment => accountPayment.AccountId == accountId &&
                                     accountPayment.Date.Year == accountBalancePeriod.Year &&
                                     accountPayment.Date.Month == accountBalancePeriod.Month)
            .Sum(accountPayment => accountPayment.Sum);

        return totalSumInPeriod;
    }
}