﻿using System.Globalization;
using System.Text.Json.Serialization;
using JfService.AccountBalanceService.Abstractions.Model;
using Newtonsoft.Json;

namespace JfService.AccountService.Implementations.Services;

/// <summary>
/// Сервис для взаимодействия с биллингом.
/// </summary>
public class BillingService : IBillingService
{
    /// <summary>
    /// Получить балансы ЛС по периодам.
    /// </summary>
    /// <param name="accountId">Идентификатор ЛС.</param>
    /// <returns></returns>
    public async Task<IEnumerable<Balance>> GetBalancesAsync(int accountId)
    {
        BalanceFileExt? balanceExts =
            JsonConvert.DeserializeObject<BalanceFileExt>(
                await File.ReadAllTextAsync("Services/Mocks/balance_202105270825.json"));

        if (balanceExts?.Balance == null)
        {
            return Array.Empty<Balance>();
        }
        
        return balanceExts.Balance
            .Where(balanceExt => balanceExt.AccountId == accountId)
            .Select(balanceExt => new Balance(
                balanceExt.AccountId,
                SafeParse(balanceExt.Period),
                balanceExt.InBalance,
                balanceExt.Calculation
            ));
    }

    private static Period SafeParse(string balanceExtPeriod)
    {
        var periodStart = DateTime.ParseExact(balanceExtPeriod, "yyyyMM", CultureInfo.InvariantCulture);
        return new Period(periodStart.Year, periodStart.Month);
    }

    /// <summary>
    /// Получить платежи.
    /// </summary>
    /// <param name="accountId">Идентификатор ЛС.</param>
    /// <returns>Платежи, отсортированные по дате возрастания. </returns>
    public async Task<IEnumerable<PaymentExt>> GetPaymentsAsync(int accountId)
    {
        PaymentExt[]? paymentExts =
            JsonConvert.DeserializeObject<PaymentExt[]>(
                await File.ReadAllTextAsync("Services/Mocks/payment_202105270827.json"));

        return paymentExts?
            .Where(payment => payment.AccountId == accountId)
            .OrderBy(payment => payment.Date).ToArray() ?? Array.Empty<PaymentExt>();
    }
}

/// <summary>
/// Баланс.
/// </summary>
public record BalanceExt
{
    /// <summary>Идентификатор ЛС.</summary>
    [JsonProperty("account_id")]
    [JsonPropertyName("account_id")]
    public int AccountId { get; init; }

    /// <summary>Период начисления в формате YYYYMM.</summary>
    [JsonProperty("period")]
    [JsonPropertyName("period")]
    public string Period { get; init; }

    /// <summary>Начальное сальдо на указанный период. Указанное значение может быть не корректным,
    /// при построении баланса рекомендуемся его рассчитывать.</summary>
    [JsonProperty("in_balance")]
    [JsonPropertyName("in_balance")]
    public Decimal InBalance { get; init; }

    /// <summary>Начислено за период.</summary>
    [JsonProperty("calculation")]
    [JsonPropertyName("calculation")]
    public Decimal Calculation { get; init; }

    /// <summary>
    /// Баланс.
    /// </summary>
    /// <param name="AccountId">Идентификатор ЛС.</param>
    /// <param name="Period">Период начисления в формате YYYYMM.</param>
    /// <param name="InBalance">Начальное сальдо на указанный период. Указанное значение может быть не корректным,
    /// при построении баланса рекомендуемся его рассчитывать.</param>
    /// <param name="Calculation">Начислено за период.</param>
    public BalanceExt(int AccountId, string Period, Decimal InBalance, Decimal Calculation)
    {
        this.AccountId = AccountId;
        this.Period = Period;
        this.InBalance = InBalance;
        this.Calculation = Calculation;
    }
}

internal record BalanceFileExt(
    [property: JsonProperty("balance")]
    [property: JsonPropertyName("balance")]
    IReadOnlyList<BalanceExt> Balance
);