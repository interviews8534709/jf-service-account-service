﻿using JfService.AccountBalanceService.Abstractions;
using JfService.AccountService.Implementations.Services;

namespace JfService.AccountService.Implementations;

/// <summary>
/// Метод расширения для <see cref="ServiceCollection"/>.
/// </summary>
public static class ServiceCollectionExtensions
{
    public static IServiceCollection RegisterServices(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<IAccountService, Services.AccountService>();
        serviceCollection.AddScoped<IBillingService, Services.BillingService>();
        return serviceCollection;
    }
}