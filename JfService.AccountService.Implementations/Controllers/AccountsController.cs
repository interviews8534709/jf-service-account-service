using JfService.AccountBalanceService.Abstractions;
using JfService.AccountBalanceService.Abstractions.Model;
using Microsoft.AspNetCore.Mvc;

namespace JfService.AccountService.Implementations.Controllers;

/// <summary>
/// Информация о лицевых счетах.
/// </summary>
[ApiController]
[Route("accounts")]
public class AccountsController : ControllerBase
{
    private readonly IAccountService _accountService;

    public AccountsController(
        IAccountService accountService,
        ILogger<AccountsController> logger)
    {
        _accountService = accountService;
    }

    /// <summary>
    /// Получить таблицу взаиморасчётов.
    /// </summary>
    /// <param name="accountId">Идентификатор лицевого счёта (ЛС).</param>
    /// <param name="periodType">Тип периода.</param>
    [HttpGet]
    [Route("{accountId}/balances")]
    [Produces("application/json", "application/xml")]
    public async Task<IActionResult> GetBalances(int accountId, PeriodType periodType = PeriodType.Month)
    {
        if (accountId <= 0)
            return BadRequest("AccountId should be greater than 0");
        
        IEnumerable<ReportItem> accountSettlementTable = 
            await _accountService.GetBalances(accountId, periodType);

        return Ok(accountSettlementTable.ToArray());
    }
}