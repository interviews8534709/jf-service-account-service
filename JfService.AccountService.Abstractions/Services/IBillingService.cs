﻿using JfService.AccountBalanceService.Abstractions.Model;

namespace JfService.AccountService.Implementations.Services;

/// <summary>
/// Сервис для взаимодействия с биллингом.
/// </summary>
public interface IBillingService
{
    /// <summary>
    /// Получить балансы ЛС по периодам.
    /// </summary>
    /// <param name="accountId">Идентификатор ЛС.</param>
    /// <returns></returns>
    Task<IEnumerable<Balance>> GetBalancesAsync(int accountId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="accountId"></param>
    /// <returns></returns>
    Task<IEnumerable<PaymentExt>> GetPaymentsAsync(int accountId);
}