﻿using JfService.AccountBalanceService.Abstractions.Model;

namespace JfService.AccountBalanceService.Abstractions;

/// <summary>
/// Сервис работы с лицевыми счетами. 
/// </summary>
public interface IAccountService
{
    /// <summary>
    /// Получить таблицу взаиморасчётов.
    /// </summary>
    /// <param name="accountId">Идентификатор лицевого счёта.</param>
    Task<IEnumerable<SettlementTableItem>> GetBalancesGroupedByMonth(int accountId);

    /// <summary>
    /// Получить таблицу взаиморасчётов по годам.
    /// </summary>
    /// <param name="accountId">Идентификатор лицевого счёта.</param>
    Task<IEnumerable<ReportItem>> GetBalancesGroupedByYear(int accountId);

    /// <summary>
    /// Получить таблицу взаиморасчётов по кварталам.
    /// </summary>
    /// <param name="accountId">Идентификатор лицевого счёта.</param>
    Task<IEnumerable<ReportItem>> GetBalancesGroupedByQuarter(int accountId);

    /// <summary>
    /// Получить таблицу взаиморасчётов в разрезе указанного типа периода.
    /// </summary>
    /// <param name="accountId">Идентификатор ЛС.</param>
    /// <param name="periodType">Тип периода.</param>
    Task<IEnumerable<ReportItem>> GetBalances(int accountId, PeriodType periodType);
}