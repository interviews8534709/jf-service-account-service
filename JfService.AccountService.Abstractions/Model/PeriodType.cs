﻿namespace JfService.AccountBalanceService.Abstractions.Model;

/// <summary>
/// Тип периода
/// </summary>
public enum PeriodType
{
    Unknown = 0,
    
    /// <summary>
    /// Месяц
    /// </summary>
    Month = 1,
    
    /// <summary>
    /// Квартал
    /// </summary>
    Quarter = 2,
    
    /// <summary>
    /// Год
    /// </summary>
    Year = 3
}