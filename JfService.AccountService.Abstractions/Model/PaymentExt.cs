﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace JfService.AccountBalanceService.Abstractions.Model;

/// <summary>
/// Информаци об платеже. 
/// </summary>
public record PaymentExt
{
   /// <summary>Идентификатор лицевого счёта.</summary>
    [JsonProperty("account_id")]
    [JsonPropertyName("account_id")]
    public int AccountId { get; init; }

    /// <summary>Дата платежа.</summary>
    [JsonProperty("date")]
    [JsonPropertyName("date")]
    public DateTimeOffset Date { get; init; }

    /// <summary>Сумма платежа.</summary>
    [JsonProperty("sum")]
    [JsonPropertyName("sum")]
    public Decimal Sum { get; init; }

    /// <summary>Уникальный идентификатор платежа.</summary>
    [JsonProperty("payment_guid")]
    [JsonPropertyName("payment_guid")]
    public Guid PaymentGuid { get; init; }
}