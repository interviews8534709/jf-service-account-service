﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace JfService.AccountBalanceService.Abstractions.Model;

/// <summary>
/// Баланс.
/// </summary>
public record Balance
{
    /// <summary>Идентификатор ЛС.</summary>
    [JsonProperty("account_id")]
    [JsonPropertyName("account_id")]
    public int AccountId { get; init; }

    /// <summary>Период начисления в формате YYYYMM.</summary>
    [JsonProperty("period")]
    [JsonPropertyName("period")]
    public Period Period { get; init; }

    /// <summary>Начальное сальдо на указанный период. Указанное значение может быть не корректным,
    /// при построении баланса рекомендуемся его рассчитывать.</summary>
    [JsonProperty("in_balance")]
    [JsonPropertyName("in_balance")]
    public Decimal InBalance { get; init; }

    /// <summary>Начислено за период.</summary>
    [JsonProperty("calculation")]
    [JsonPropertyName("calculation")]
    public Decimal Calculation { get; init; }

    /// <summary>
    /// Баланс.
    /// </summary>
    /// <param name="accountId">Идентификатор ЛС.</param>
    /// <param name="period">Период начисления в формате YYYYMM.</param>
    /// <param name="inBalance">Начальное сальдо на указанный период. Указанное значение может быть не корректным,
    /// при построении баланса рекомендуемся его рассчитывать.</param>
    /// <param name="calculation">Начислено за период.</param>
    public Balance(int accountId, Period period, Decimal inBalance, Decimal calculation)
    {
        this.AccountId = accountId;
        this.Period = period;
        this.InBalance = inBalance;
        this.Calculation = calculation;
    }
}