﻿namespace JfService.AccountBalanceService.Abstractions.Model;

/// <summary>
/// Описывает период.
/// </summary>
public readonly struct Period: IComparable<Period> 
{
    /// <summary>
    /// Год.
    /// </summary>
    public int Year { get; init; }

    /// <summary>
    /// Месяц.
    /// </summary>
    public int Month { get; init; }

    /// <summary>
    /// Квартал, которому принадлежит период.
    /// </summary>
    public int Quarter => (Month - 1) / 3 + 1;

    /// <summary>
    /// Период.
    /// </summary>
    /// <param name="year">Год.</param>
    /// <param name="month">Месяц.</param>
    public Period(int year, int month)
    {
        Year = year;
        Month = month;
    }

    public int CompareTo(Period other)
    {
        if (Year == other.Year && Month == other.Month)
            return 0;
        
        if (Year > other.Year || 
            Year == other.Year && Month > other.Month)
            return 1;

        return -1;
    }

    public override string ToString()
    {
        return $"{Year}{Month}";
    }
}