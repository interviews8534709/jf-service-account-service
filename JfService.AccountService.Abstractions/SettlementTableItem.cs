﻿using JfService.AccountBalanceService.Abstractions.Model;

namespace JfService.AccountBalanceService.Abstractions;

/// <summary>
/// Запись таблицы взаиморасчётов.
/// </summary>
public class SettlementTableItem
{
    /// <summary>
    /// Наименование периода.
    /// </summary>
    public Period Period { get; set;}

    /// <summary>
    /// Входящее сальдо на начало периода.
    /// </summary>
    public Decimal InBalance { get; set;}

    /// <summary>
    /// Начислено за период.
    /// </summary>
    public Decimal Calculation { get; set;}

    /// <summary>
    /// Оплачено за период.
    /// </summary>
    public Decimal Payment { get; set;}

    /// <summary>
    /// Исходящее сальдо на конец периода.
    /// </summary>
    public Decimal OutBalance { get; set;}

    public SettlementTableItem()
    {
        
    }
    
    /// <summary>
    /// Запись таблицы взаиморасчётов.
    /// </summary>
    public SettlementTableItem(Period period, decimal inBalance, decimal calculation, decimal payment, decimal outBalance)
    {
        Period = period;
        InBalance = inBalance;
        Calculation = calculation;
        Payment = payment;
        OutBalance = outBalance;
    }
}